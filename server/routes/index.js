module.exports = [
  {
    method: "GET",
    path: "/",
    handler: "myController.index",
    config: {
      policies: [],
      auth: false,
    },
  },
  {
    method: "POST",
    path: "/export-data",
    handler: "myController.exportData",
    config: {
      policies: [],
    },
  },
];
