"use strict";

module.exports = ({ strapi }) => ({
  index(ctx) {
    ctx.body = strapi
      .plugin("export-data")
      .service("myService")
      .getWelcomeMessage();
  },

  async exportData(ctx) {
    ctx.body = await strapi.plugin("export-data").service("myService").exportData(ctx.request.body.data);
  },
});
