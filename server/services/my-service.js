"use strict";
// import qs from 'qs';

module.exports = ({ strapi }) => ({
  getWelcomeMessage() {
    return "Welcome to Strapi 🚀";
  },

  // uid: api::subscriber.subscriber
  async exportData(options) {
    const records = await strapi.entityService.findMany(options.uid, options.params);
    return records;
  },
});
