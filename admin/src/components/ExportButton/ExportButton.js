import { Button } from "@strapi/design-system";
import { CheckPermissions } from "@strapi/helper-plugin";
import Download from "@strapi/icons/Download";
import React, { useMemo, useState } from "react";
import { useIntl } from "react-intl";
import qs from "qs";
import pick from "lodash/pick";
import { useLocation } from "react-router-dom";

import getTrad from "../../utils/getTrad";
import { useFetchClient } from "@strapi/helper-plugin";
import Papa from "papaparse";

// this should get from plugin config
const enableUids = ["api::subscriber.subscriber"];

export const ExportButton = ({
  availableExportFormats,
  unavailableOptions,
  fullWidth = false,
}) => {
  const [isLoading, setLoading] = useState(false);
  const { post } = useFetchClient();
  const { formatMessage } = useIntl();
  const { search, pathname } = useLocation();

  const uid = useMemo(() => {
    const b = "/content-manager/collection-types/";
    const p = pathname + "";
    if (!p.includes(b)) {
      return;
    }
    const r = p.replace(b, "");
    if (!enableUids.includes(r)) {
      return;
    }
    return r;
  }, [pathname]);

  const getData = () => {
    setLoading(true);
    post("/export-data/export-data", {
      data: {
        uid: "api::subscriber.subscriber",
        params: {
          ...pick(qs.parse(search), ["filters", "sort"]),
          start: 0,
          limit: 10000,
        },
      },
    })
      .then((response) => {
        const csvData = Papa.unparse(response.data);
        downloadCSV(csvData, "data.csv");
      })
      .catch((error) => {
        alert("Cannot create file to download.");
        console.error(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  if (!uid) {
    return null;
  }

  return (
    // @ts-ignore
    <Button
      startIcon={<Download />}
      fullWidth={fullWidth}
      onClick={getData}
      disabled={isLoading}
    >
      {formatMessage({ id: getTrad("plugin.cta.export") })}
    </Button>
  );
};

function downloadCSV(data, filename) {
  const csvContent = "\uFEFF" + data; // Add BOM character to support UTF-8 encoding
  const blob = new Blob([csvContent], { type: "text/csv;charset=utf-8;" });
  const link = document.createElement("a");
  if (link.download !== undefined) {
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", filename);
    link.style.visibility = "hidden";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}